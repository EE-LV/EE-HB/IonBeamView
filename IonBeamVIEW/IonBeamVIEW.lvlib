﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="11008008">
	<Property Name="Alarm Database Computer" Type="Str">localhost</Property>
	<Property Name="Alarm Database Name" Type="Str">E__Program_Files_National_Instruments_LabVIEW_8_6_data</Property>
	<Property Name="Alarm Database Path" Type="Str">E:\Program Files\National Instruments\LabVIEW 8.6\data</Property>
	<Property Name="Data Lifespan" Type="UInt">3650</Property>
	<Property Name="Database Computer" Type="Str">localhost</Property>
	<Property Name="Database Name" Type="Str">E__Program_Files_National_Instruments_LabVIEW_8_6_data</Property>
	<Property Name="Database Path" Type="Str">E:\Program Files\National Instruments\LabVIEW 8.6\data</Property>
	<Property Name="Enable Alarms Logging" Type="Bool">true</Property>
	<Property Name="Enable Data Logging" Type="Bool">true</Property>
	<Property Name="NI.Lib.Description" Type="Str">This library contains the IonBeamVIEW Application VI's.

Author: H.Brand@gsi.de

History:
Revision 0.0.0.0: Feb 21, 2009 H.Brand@gsi.de; start of development.

Copyright 2009 GSI

Gesellschaft für Schwerionenforschung mbH
Planckstr. 1, 64291 Darmstadt, Germany
Contact: H.Brand@gsi.de

This file is part of the IonBeamVIEW project.

    IonBeamVIEW project is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    IonBeamVIEW project is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with IonBeamVIEW project.  If not, see http://www.gnu.org/licenses/.</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!)C!!!*Q(C=\&gt;7^53."%-8RRR8'O=LA3CFU!BB+13GUDS585SE]$XPL-F!+H9*35!LC0U-@BQ')+K$O$(996HLT^7.W;J(G^6/[VPH3&gt;@PG^9\W]_QU\K_W&gt;`H)_*?&lt;`YZ`P@W3`Y0N&amp;SZ^8^`8P\XO,B\3,T_T6RIP)GJ245VK;#RV,P)C,`)C,`)C.\H*47ZSEZM]S:-]S:-]S:-]S)-]S)-]S).]&amp;H+2CVTEE)L&amp;CY7+39M*CMZ160QK0)7H]"1?PKLQ&amp;*\#5XA+$VV5?!J0Y3E]B9&gt;B+DS&amp;J`!5HM,$6&amp;.3MZ$D+4R-,_-R(O-R(O.B32G0!:D&amp;T-2G%BASD?;$]2C0]@"2RG-]RG-]RE/TD-&gt;YD-&gt;YD)=B=V=].;/1YW%;*:\%EXA34_*B;C7?R*.Y%E`C94EFHM34)*)&amp;E]EB+"G5&gt;%C_**\%QUW**`%EHM34?'C;4SDHTAT.+/2Y!E`A#4S"*`!QB1*0Y!E]A3@Q-+U#4_!*0)%H],#5!E`A#4Q"*&amp;C5Z26-&amp;AQ-/A6"Y/&amp;HHJ;94]F4%L.)@8D6BV*^W.3(3(UYV!^&gt;`4$6$UG^_?J.67_7?B05@ZQ;L=;I&amp;V%0(BVVYP?2?K!OV$VV2^V3.^1V&gt;47'@H,(U_GEY`'IQ_'A:6GUX__VW_WUX7[VW7SU8K_V7KW?8A/`+%]PB-@XUBW@2`V^L[PFPO]XX']?]T`VC`[8`A@P2PX1]XH:IQ?&lt;O,J_!!!!!!</Property>
	<Property Name="NI.Lib.LocalName" Type="Str">IonBeamVIEW</Property>
	<Property Name="NI.Lib.Version" Type="Str">0.0.0.0</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Property Name="SaveStatePeriod" Type="UInt">0</Property>
	<Property Name="Serialized ACL" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!A1%!!!@````]!!".V&lt;H.J:WZF:#"C?82F)'&amp;S=G&amp;Z!!%!!1!!!!A)!!!!#!!!!!!!!!!</Property>
	<Property Name="Use Data Logging Database" Type="Bool">true</Property>
	<Item Name="subVIs" Type="Folder">
		<Item Name="Analysis.vi" Type="VI" URL="../Analysis.vi"/>
		<Item Name="readCamerasFromConfigurationFile.vi" Type="VI" URL="../readCamerasFromConfigurationFile.vi"/>
		<Item Name="CalcRelativeYields.vi" Type="VI" URL="../CalcRelativeYields.vi"/>
	</Item>
	<Item Name="Typedefs" Type="Folder">
		<Item Name="DAQ State.ctl" Type="VI" URL="../TypeDefs/DAQ State.ctl"/>
		<Item Name="DAQ Command.ctl" Type="VI" URL="../TypeDefs/DAQ Command.ctl"/>
		<Item Name="Camera Parameters.ctl" Type="VI" URL="../TypeDefs/Camera Parameters.ctl"/>
		<Item Name="Configuration.ctl" Type="VI" URL="../TypeDefs/Configuration.ctl"/>
	</Item>
	<Item Name="Main_OO.vi" Type="VI" URL="../Main_OO.vi"/>
	<Item Name="IonBeamVIEW_OO.rtm" Type="Document" URL="../IonBeamVIEW_OO.rtm"/>
	<Item Name="IonBeamVIEW.ini" Type="Document" URL="../IonBeamVIEW.ini"/>
</Library>
